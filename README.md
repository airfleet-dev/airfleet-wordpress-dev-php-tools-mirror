# Airfleet WordPress Dev PHP Tools

[![Packagist Version](https://img.shields.io/packagist/v/airfleet/wordpress-dev)](https://packagist.org/packages/airfleet/wordpress-dev)
[![pipeline status](https://gitlab.com/codersclan/tools/airfleet-wordpress-dev-php-tools/badges/main/pipeline.svg)](https://gitlab.com/codersclan/tools/airfleet-wordpress-dev-php-tools/-/commits/main)

Composer package with tools and configs for WordPress development.

## Documentation

- [Documentation](https://www.notion.so/codersclan/WordPress-Dev-PHP-Tools-3d653d14a38b425d8d9ef5fc955f514c).

## License

© [Airfleet](https://www.airfleet.co/)
